import React from 'react';
import { render } from 'react-dom';
import TestComponent from './component/Component';

render(
  <TestComponent />,
  document.querySelector('#app')
);
