import React, { Component } from 'react';
import styles from './style.scss';

class TestComponent extends Component {
  render() {
    return (
      <div className="test">Test Component</div>
    );
  }
}

export default TestComponent;
